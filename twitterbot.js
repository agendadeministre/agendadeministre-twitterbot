const fs = require('fs');
const path = require('path');
const Twit = require('twit');
const moment = require('moment');
var T;

var twitterbot = async function () {
  global.twitter_api_key = process.env.TWITTER_API_KEY;
  global.twitter_api_key_secret = process.env.TWITTER_API_KEY_SECRET;
  global.twitter_access_token = process.env.TWITTER_ACCESS_TOKEN;
  global.twitter_access_token_secret = process.env.TWITTER_ACCESS_TOKEN_SECRET;

  if (!global.twitter_api_key || !global.twitter_api_key_secret || !global.twitter_access_token || !global.twitter_access_token_secret) {
    console.log("Error: Twitter API keys not found!");
    return;
  }

  T = new Twit({
    consumer_key: global.twitter_api_key,
    consumer_secret: global.twitter_api_key_secret,
    access_token: global.twitter_access_token,
    access_token_secret: global.twitter_access_token_secret,
    timeout_ms: 60 * 1000,
    strictSSL: true,
  });

  var calendarData = [];
  var gouv = [];
  var gouvSearch = {};
  var filePath = path.join(__dirname, 'data/gouv.json');
  var gouvData = await fs.promises.readFile(filePath, {
    encoding: 'utf-8'
  })
  gouv = JSON.parse(gouvData);
  for (g in gouv) {
    gouvSearch[gouv[g].id] = gouv[g];
  }
  for (g in gouv) {
    if (gouv[g].dispo == 1) {
      try {
        const data = await fs.promises.readFile('data/' + gouv[g].id + '.json');
        calendarData = calendarData.concat(JSON.parse(data));
      } catch (err) {
        console.log(err);
      }

    }
  }

  var tweets = generateTweets(calendarData, moment().hours(15).minutes(0).seconds(0));

  console.log(tweets);
  postTweet(tweets, 0, undefined);
}

function generateTweets(calendarData, now) {
  var eventsNow = [];
  for (c in calendarData) {
    if (calendarData[c]) {
      var eventTime = moment(calendarData[c].start);
      var eventDiff = eventTime.diff(now, "seconds");
      if (eventDiff < 30 && eventDiff >= -30) {
        eventsNow.push(calendarData[c]);
      }
    }
  }

  var tweet = "";
  if (eventsNow.length > 0) {
    if (eventsNow.length == 1) {
      tweet += "🔔 Nouvel événement !";
    } else {
      tweet += "🔔 " + eventsNow.length + " nouveaux événements !";
    }
    for (e in eventsNow) {
      var beforeTitle = "";
      var title = "";
      var afterTitle = "";
      var attendees = "";
      if (eventsNow[e].attendees.length < 5) {
        for (a in eventsNow[e].attendees) {
          if (a == 0) {
            attendees += eventsNow[e].attendees[a];
          } else if (a == eventsNow[e].attendees.length - 1) {
            attendees += " et " + eventsNow[e].attendees[a];
          } else {
            attendees += ", " + eventsNow[e].attendees[a];
          }
        }
        beforeTitle = "📆 Dans l'agenda de " + attendees + " : ";
      } else {
        beforeTitle = "📆 Dans l'agenda de plusieurs ministres : ";
      }
      title = eventsNow[e].title;
      if (eventsNow[e].location) {
        var location = eventsNow[e].location;
        if (location.length > 140) {
          location = location.slice(0, 140) + "[…]";
        }
        afterTitle = " (" + location + ")";
      }
      if (beforeTitle.length + title.length + afterTitle.length > 280) {
        title = title.slice(0, 280 - (beforeTitle.length + title.length + afterTitle.length) - 3) + "[…]"
      }
      if (tweet != "") {
        tweet += "\n";
      }
      tweet += beforeTitle + title + afterTitle;
    }

    var tweets = [];
    while (tweet.length > 280) {
      var tweetSplit = tweet.split("\n");
      var tweetMerged = "";
      var previousTweetMerged = "";
      for (var m = 0; m < tweetSplit.length; m++) {
        tweetMerged += tweetSplit[m];
        if (tweetMerged.length > 280) {
          tweet = tweet.replace(previousTweetMerged + "\n", "");
          tweets.push(previousTweetMerged);
          break;
        } else {
          previousTweetMerged = tweetMerged;
          tweetMerged += "\n";
        }
      }
    }
    tweets.push(tweet);
  }

  return tweets;
}

function postTweet(tweets, t, in_reply_to_status_id) {
  if (tweets[t]) {
    if (t == 0 && !in_reply_to_status_id || t != 0) {
      T.post('statuses/update', {
        status: tweets[t],
        in_reply_to_status_id: in_reply_to_status_id
      }, function (err, data, response) {
        if (err) {
          console.log(err);
        } else if (data.id_str) {
          postTweet(tweets, t + 1, data.id_str);
        }
      });
    } else {
      console.log("Error: in_reply_to_status_id not found");
    }
  }
}

exports.execute = twitterbot;
exports.generateTweets = generateTweets;