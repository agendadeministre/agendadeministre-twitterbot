const twitter = require('./twitterbot');
const moment = require('moment');

test('generate array of tweets from calendar data', () => {
	var calendarData = [
		{
			title: "Test 1 avec un pas mal très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très trèstrès très très très logn titre",
			location: "Ici",
			start: "2020-07-01T18:00:00.000Z",
			attendees: ["Moi moi", "Autre personne", "Encore quelqu'un"]
		},
		{
			title: "Test 2",
			location: "",
			start: "2020-07-01T18:00:00.000Z",
			attendees: ["Moi moi", "Autre personne"]
		},
		{
			title: "Test 3 avec un très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très trèstrès très très très très très très très très trèstrès très très très très très très très très trèstrès très très très très très très très très trèstrès très très très très très très très très trèstrès très très très très très très très très très long titre",
			location: "",
			start: "2020-07-01T18:00:00.000Z",
			attendees: ["Moi moi"]
		},
		{
			title: "Test 4",
			location: "Et un emplacement super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super super long",
			start: "2020-07-01T18:00:00.000Z",
			attendees: ["Moi moi", "Autre personne"]
		},
		{
			title: "Test 5",
			location: "Et un emplacement",
			start: "2020-07-01T18:00:00.000Z",
			attendees: ["Moi 1", "Moi 2", "Moi 3", "Moi 4", "Moi 5", "Moi 6", "Moi 7", "Moi 8"]
		},
		{
			title: "Should not be in tweets",
			location: "",
			start: "2020-07-01T19:00:00.000Z",
			attendees: ["Moi moi"]
		}
	];

	var expectedResults =     [
      '🔔 5 nouveaux événements !',
      "📆 Dans l'agenda de Moi moi, Autre personne et Encore quelqu'un : Test 1 avec un pas mal très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très tr[…] (Ici)",
      "📆 Dans l'agenda de Moi moi et Autre personne : Test 2",
      "📆 Dans l'agenda de Moi moi : Test 3 avec un très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très très trèstrès très très très très très très trè[…]",
      "📆 Dans l'agenda de Moi moi et Autre personne : Test 4 (Et un emplacement super super super super super super super super super super super super super super super super super super super super su[…])\n" +
        "📆 Dans l'agenda de plusieurs ministres : Test 5 (Et un emplacement)"
    ];
	
	var results = twitter.generateTweets(calendarData, moment("2020-07-01T18:00:00.000Z"));

	expect(results).toStrictEqual(expectedResults);
});